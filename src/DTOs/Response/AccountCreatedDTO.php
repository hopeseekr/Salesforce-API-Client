<?php declare(strict_types=1);

namespace PHPExperts\SalesforceClient\DTOs\Response;

use PHPExperts\SimpleDTO\SimpleDTO;

/**
 * @property-read string   $id
 * @property-read bool     $success
 * @property-read string[] $errors
 */
class AccountCreatedDTO extends SimpleDTO
{
}
