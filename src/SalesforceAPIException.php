<?php declare(strict_types=1);

namespace PHPExperts\SalesforceClient;

use RuntimeException;

class SalesforceAPIException extends RuntimeException
{
    public const API_AUTH   = 1;
    public const API_GET    = 2;
    public const API_POST   = 3;
    public const API_PUT    = 4;
    public const API_DELETE = 5;
}
