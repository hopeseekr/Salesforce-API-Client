<?php declare(strict_types=1);

namespace PHPExperts\SalesforceClient\DTOs;

use Carbon\Carbon;
use PHPExperts\DataTypeValidator\DataTypeValidator;
use PHPExperts\SimpleDTO\NestedDTO;
use PHPExperts\SimpleDTO\WriteOnce;

/**
 * @property string      $Id                       // "001i000000ROZvdAAH"
 * @property bool        $IsDeleted                // false
 * @property null|string $MasterRecordId           // null
 * @property string      $Name                     // "Michael (Mike) Alexander"
 * @property string      $LastName                 // "Alexander"
 * @property string      $FirstName                // "Michael (Mike)"
 * @property null|string $Salutation               // null
 * @property string      $Type                     // "Member"
 * @property string      $RecordTypeId             // "012i00000007A6xAAE"
 * @property null|string $ParentId                 // null
 * @property string      $BillingStreet            // "405 Fairbanks"
 * @property string      $BillingCity              // "Houston"
 * @property string      $BillingState             // "Texas"
 * @property string      $BillingPostalCode        // "77009"
 * @property string      $BillingCountry           // "United States"
 * @property string      $BillingStateCode         // "TX"
 * @property string      $BillingCountryCode       // "US"
 * @property null|?float $BillingLatitude          // null
 * @property null|?float $BillingLongitude         // null
 * @property null|float  $BillingGeocodeAccuracy   // null
 * @property AddressDTO  $BillingAddress
 * @property string      $ShippingStreet           // "405 Fairbanks"
 * @property string      $ShippingCity             // "Houston"
 * @property string      $ShippingState            // "Texas"
 * @property string      $ShippingPostalCode       // "77009"
 * @property string      $ShippingCountry          // "United States"
 * @property string      $ShippingStateCode        // "TX"
 * @property string      $ShippingCountryCode      // "US"
 * @property null|?float $ShippingLatitude         // null
 * @property null|?float $ShippingLongitude        // null
 * @property null|float  $ShippingGeocodeAccuracy  // null
 * @property AddressDTO  $ShippingAddress
 * @property string      $Phone                    // "832-758-8811"
 * @property null|string $Fax                      // null
 * @property string      $AccountNumber            // "521029a1-2de3-4a92-9a43-263ffd366a8b"
 * @property null|string $Website                  // null
 * @property string      $PhotoUrl                 // "/services/images/photo/001i000000ROZvdAAH"
 * @property null|string $Sic                      // null
 * @property null|string $Industry                 // null
 * @property null|float  $AnnualRevenue            // null
 * @property null|int    $NumberOfEmployees        // null
 * @property null|string $Ownership                // null
 * @property null|string $TickerSymbol             // null
 * @property null|string $Description              // null
 * @property null|string $Rating                   // null
 * @property null|string $Site                     // null
 * @property string      $OwnerId                  // "005i0000001TzfqAAC"
 * @property Carbon      $CreatedDate              // "2013-10-19T01:15:27.000+0000"
 * @property string      $CreatedById              // "005i0000001TzfqAAC"
 * @property Carbon      $LastModifiedDate         // "2019-04-01T15:25:41.000+0000"
 * @property string      $LastModifiedById         // "00531000006UpLWAA0"
 * @property Carbon      $SystemModstamp           // "2019-04-01T15:25:41.000+0000"
 * @property string      $LastActivityDate         // "2017-05-19"
 * @property Carbon      $LastViewedDate           // "2019-06-28T04:59:18.000+0000"
 * @property Carbon      $LastReferencedDate       // "2019-06-28T04:59:18.000+0000"
 * @property bool        $IsExcludedFromRealign    // false
 * @property string      $PersonContactId          // "003i000000NYctAAAT"
 * @property bool        $IsPersonAccount          // true
 * @param string         $PersonMailingStreet          // "405 Fairbanks"
 * @param string         $PersonMailingCity            // "Houston"
 * @param string         $PersonMailingState           // "Texas"
 * @param string         $PersonMailingPostalCode      // "77009"
 * @param string         $PersonMailingCountry         // "United States"
 * @param string         $PersonMailingStateCode       // "TX"
 * @param string         $PersonMailingCountryCode     // "US"
 * @param null|float     $PersonMailingLatitude        // null
 * @param null|float     $PersonMailingLongitude       // null
 * @param null|float     $PersonMailingGeocodeAccuracy // null
 * @property AddressDTO  $PersonMailingAddress
 * @property null|string $PersonMobilePhone        // null
 * @property null|string $PersonOtherPhone         // null
 * @property null|string $PersonAssistantPhone     // null
 * @property null|string $PersonEmail              // "alexandermichaelwinston@gmail.com"
 * @property null|string $PersonTitle              // null
 * @property null|string $PersonDepartment         // null
 * @property null|string $PersonAssistantName      // null
 * @property null|string $PersonLeadSource         // null
 * @property null|string $PersonBirthdate          // null
 * @property null|bool   $PersonHasOptedOutOfEmail // false
 * @property null|bool   $PersonDoNotCall          // false
 * @property null|string $PersonLastCURequestDate  // null
 * @property null|string $PersonLastCUUpdateDate   // null
 * @property null|string $PersonEmailBouncedReason // null
 * @property null|string $PersonEmailBouncedDate   // null
 * @property null|string $PersonIndividualId       // null
 * @property null|string $Jigsaw                   // null
 * @property null|string $JigsawCompanyId          // null
 * @property null|string $AccountSource            // "Web"
 * @property null|string $SicDesc                  // null
 */
class AccountDTO extends NestedDTO
{
    use WriteOnce;

    public function __construct(array $input = [], array $options = null, DataTypeValidator $validator = null)
    {
        $DTOs = [
            'BillingAddress'       => AddressDTO::class,
            'ShippingAddress'      => AddressDTO::class,
            'PersonMailingAddress' => AddressDTO::class,
        ];

        $newput = array_filter($input);
        $DTOs = array_intersect_key($DTOs, $newput);

        parent::__construct($input, $DTOs, $options ?? [NestedDTO::PERMISSIVE], $validator);
    }
}
