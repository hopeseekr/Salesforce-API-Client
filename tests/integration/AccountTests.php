<?php declare(strict_types=1);

namespace PHPExperts\SalesforceClient\Tests\Integration;

use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use PHPExperts\DataTypeValidator\InvalidDataTypeException;
use PHPExperts\SalesforceClient\DTOs\AccountDTO;
use PHPExperts\SalesforceClient\DTOs\AddressDTO;
use PHPExperts\SalesforceClient\DTOs\Response\AccountCreatedDTO;
use PHPExperts\SalesforceClient\SalesforceClient;
use PHPExperts\SalesforceClient\Tests\TestCase;

class AccountTests extends TestCase
{
    // Mike Alexander's account.
    private const CRMID = '001i000000ROZvdAAH';

    /** @var SalesforceClient */
    private $sfClient;

    public function setUp(): void
    {
        $this->sfClient = new SalesforceClient();

        parent::setUp();
    }

    public function testCanCreateAccount()
    {
        $address = [
            'street'      => '2508 Green Tee Dr',
            'city'        => 'Pearland',
            'stateCode'   => 'TX',
            'postalCode'  => '77581',
            'countryCode' => 'US',
        ];

        $account = new AccountDTO([
            'FirstName'            => 'Theodore',
            'LastName'             => 'Smith',
            'BillingStreet'        => $address['street'],
            'BillingCity'          => $address['city'],
            'BillingStateCode'     => $address['stateCode'],
            'BillingPostalCode'    => $address['postalCode'],
            'BillingCountryCode'   => $address['countryCode'],
            'ShippingStreet'       => $address['street'],
            'ShippingCity'         => $address['city'],
            'ShippingStateCode'    => $address['stateCode'],
            'ShippingPostalCode'   => $address['postalCode'],
            'ShippingCountryCode'  => $address['countryCode'],
            'Phone'                => '832-303-9477',
            'PersonEmail'          => 'theodore@phpexperts.pro',
        ]);

        $response = $this->sfClient->account->store($account);
        self::assertInstanceOf(AccountCreatedDTO::class, $response);
        self::assertTrue($response->success);
        self::assertIsString($response->id);
        self::assertEmpty($response->errors);

        return $response->id;
    }

    /** @depends testCanCreateAccount */
    public function testCanShowAccount(string $crmId)
    {
        $response = $this->sfClient->account->id($crmId)->show();

        self::assertEquals($crmId, $response->Id);
        self::assertEquals('Theodore', $response->FirstName);
        self::assertEquals('Smith', $response->LastName);
        self::assertEquals('theodore@phpexperts.pro', $response->PersonEmail);
        self::assertInstanceOf(Carbon::class, $response->CreatedDate);
    }

    /** @depends testCanCreateAccount */
    public function testCanUpdateAccount(string $crmId)
    {
        $updateDTO = new AccountDTO();
        $updateDTO->Phone = '832-303-9477';
        $updateDTO->BillingCity = 'New York';
        $updateDTO->BillingStateCode = 'NY';
        $updateDTO->BillingCountryCode = 'US';

        $response = $this->sfClient->account->id($crmId)->update($updateDTO);
        self::assertTrue($response);
    }

    /** @depends testCanCreateAccount */
    public function testCanDeleteAccount(string $crmId)
    {
        $response = $this->sfClient->account->id($crmId)->destroy();
        self::assertTrue($response, 'Could not delete a Salesforce account.');
    }
}
