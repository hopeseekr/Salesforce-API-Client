<?php

return [
    'src/MyPackage.php'                => 'src/SalesforceClient.php',
    'config/mypackage.php'             => 'config/salesforceclient.php',
    'src/MyPackageServiceProvider.php' => 'src/SalesforceRestClientServiceProvider.php',
];
