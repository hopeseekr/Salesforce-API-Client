<?php declare(strict_types=1);

/**
 * This file is part of the Salesforce PHP API Client, a PHP Experts, Inc., Project.
 *
 * Copyright © 2019 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *  GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *  https://www.phpexperts.pro/
 *  https://github.com/phpexpertsinc/Salesforce-API-Client
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\SalesforceClient\Tests;

use Dotenv\Dotenv;
use Faker\Factory as Faker;
use PHPUnit\Framework\TestCase as BaseTestCase;

/**
 * Any new tests that use the Laravel 5.4 testing layer will extend this TestCase class.
 */
abstract class TestCase extends BaseTestCase
{
    /**
     * Constructs a test case with the given name.
     *
     * @param string $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->faker = Faker::create();

        $dotenv = Dotenv::create(__DIR__ . '/../', '.env.testing');
        $dotenv->load();
    }
}