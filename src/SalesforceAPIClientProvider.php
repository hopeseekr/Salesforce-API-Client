<?php declare(strict_types=1);

/**
 * This file is part of the Salesforce PHP API Client, a PHP Experts, Inc., Project.
 *
 * Copyright © 2019 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *  GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *  https://www.phpexperts.pro/
 *  https://github.com/phpexpertsinc/Salesforce-API-Client
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\SalesforceClient;

use Illuminate\Support\ServiceProvider;
use PHPExperts\RESTSpeaker\RESTAuth;

class SalesforceAPIClientProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Makes use of the Factory pattern.
        $this->app->singleton('salesforce', function ($app) {
            if (env('APP_ENV') === 'prod') {
                $restAuth = new RESTAuthStrat(RESTAuth::AUTH_MODE_OAUTH2);
            } else {
                $restAuth = new RESTAuthStrat(RESTAuth::AUTH_MODE_PASSKEY);
            }

            $salesforceClient = new SalesforceClient($restAuth);

            return $salesforceClient;
        });

        // To Access:
        //    $salesforceClient = App::make('salesforce');
    }
}
