<?php declare(strict_types=1);

/**
 * This file is part of the Salesforce PHP API Client, a PHP Experts, Inc., Project.
 *
 * Copyright © 2019 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *  GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *  https://www.phpexperts.pro/
 *  https://github.com/phpexpertsinc/Salesforce-API-Client
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\SalesforceClient\Managers;

use GuzzleHttp\Exception\ClientException;
use Koriym\HttpConstants\StatusCode as HTTP;
use PHPExperts\SalesforceClient\DTOs\AccountDTO;
use PHPExperts\SalesforceClient\DTOs\Response\AccountCreatedDTO;
use PHPExperts\SalesforceClient\SalesforceAPIException;

class Account extends Manager
{
    public function show(): AccountDTO
    {
        $this->assertHasId();
        $crmId = $this->id;
        $response = $this->api->get("sobjects/Account/{$crmId}");

        return new AccountDTO((array)$response);
    }

    public function store(AccountDTO $account): AccountCreatedDTO
    {
        $response = $this->api->post('sobjects/Account', [
            'json' => $account,
        ]);

        if ($this->api->getLastStatusCode() !== HTTP::CREATED ||
            ($response->success ?? false) !== true) {
            $errorMsg = 'Could not create a Salesforce account, probably due to an API break.';

            throw new SalesforceAPIException($errorMsg, SalesforceAPIException::API_PUT);
        }

        return new AccountCreatedDTO((array) $response);
    }

    public function update(AccountDTO $account): bool
    {
        $this->assertHasId();
        $crmId = $this->id;

        $this->api->patch("sobjects/Account/{$crmId}", [
            'json' => $account,
        ]);

        if ($this->api->getLastStatusCode() !== HTTP::NO_CONTENT) {
            $errorMsg = "Could not update the Salesforce record with crmId '$crmId'.";

            throw new SalesforceAPIException($errorMsg, SalesforceAPIException::API_PUT);
        }

        return true;
    }

    public function destroy()
    {
        $this->assertHasId();
        $crmId = $this->id;

        try {
            $this->api->delete("/services/data/v45.0/sobjects/Account/{$crmId}");
        }
        catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === HTTP::NOT_FOUND) {
                return true;
            }

            $errorMsg = "Could not destroy the Salesforce record with crmId '$crmId': ";

            throw new SalesforceAPIException($errorMsg . $e->getMessage(), SalesforceAPIException::API_DELETE);
        }

        return true;
    }
}
