<?php declare(strict_types=1);

/**
 * This file is part of the Salesforce PHP API Client, a PHP Experts, Inc., Project.
 *
 * Copyright © 2019 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *  GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *  https://www.phpexperts.pro/
 *  https://github.com/phpexpertsinc/Salesforce-API-Client
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\SalesforceClient\Managers;

use LogicException;
use PHPExperts\RESTSpeaker\RESTSpeaker;
use PHPExperts\SalesforceClient\SalesforceClient;

abstract class Manager
{
    /** @var RESTSpeaker */
    protected $api;

    /** @var SalesforceClient */
    protected $salesforce;

    /** @var string */
    protected $id;

    public function __construct(SalesforceClient $salesforce, RESTSpeaker $api)
    {
        $this->salesforce = $salesforce;
        $this->api = $api;
    }

    /**
     * @param string $id
     * @return static
     */
    public function id(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    protected function assertHasId()
    {
        if ($this->id === null) {
            throw new LogicException('An ID must be set for ' . self::class . '.');
        }
    }
}
