<?php declare(strict_types=1);

/**
 * This file is part of the Salesforce PHP API Client, a PHP Experts, Inc., Project.
 *
 * Copyright © 2019 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *  GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *  https://www.phpexperts.pro/
 *  https://github.com/phpexpertsinc/Salesforce-API-Client
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\SalesforceClient;

use GuzzleHttp\Client as Guzzle_Client;
use LogicException;
use PHPExperts\RESTSpeaker\RESTAuth as BaseRESTAuth;
use RuntimeException;

class RESTAuthStrat extends BaseRESTAuth
{
    /** @var string */
    protected $authMode;

    public function __construct(string $authStratMode)
    {
        if (!in_array($authStratMode, [self::AUTH_MODE_PASSKEY, self::AUTH_MODE_OAUTH2])) {
            throw new LogicException('Invalid Salesforce REST API auth mode.');
        }

        parent::__construct($authStratMode);
    }

    /**
     * @throws LogicException if token auth is attempted on an unsupported Salesforce API environment.
     * @throws RuntimeException if an OAuth2 Token could not be successfully generated.
     * @return array The appropriate headers for OAuth2 Tokens.
     */
    protected function generateOAuth2TokenOptions(): array
    {
        $response = (new Guzzle_Client())->post(env('SALESFORCE_LOGIN_URL'), [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
            ],
        ]);
        $response = json_decode($response->getBody()->getContents());

        if (!$response || empty($response->access_token)) {
            throw new RuntimeException('Could not generate an OAuth2 Token for Salesforce.');
        }

        return [
            'headers' => [
                'Authorization' => "bearer {$response->access_token}",
            ],
        ];
    }

    /**
     * @throws LogicException because the method is not implemented yet.
     * @return array The appropriate headers for passkey authorization.
     */
    protected function generatePasskeyOptions(): array
    {
        throw new \LogicException('Not Implemented.');
    }
}
