<?php declare(strict_types=1);

namespace PHPExperts\SalesforceClient;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Response;
use PHPExperts\RESTSpeaker\RESTAuth;
use PHPExperts\RESTSpeaker\RESTSpeaker;

class SalesforceRESTAuth extends RESTAuth
{
    public const SALESFORCE_VERSION = 'v45.0';

    /** @var string */
    protected $instanceURL = '';

    public function __construct(RESTSpeaker $apiClient = null)
    {
        parent::__construct(RESTAuth::AUTH_MODE_OAUTH2, $apiClient);
    }

    public function getInstanceURL(): string
    {
        return $this->instanceURL . '/services/data/' . self::SALESFORCE_VERSION . '/';
    }

    public function login(): self
    {
        $this->obtainOAuthToken();

        return $this;
    }

    protected function generatePasskeyOptions(): array
    {
        return [];
    }

    protected function obtainOAuthToken(): string
    {
        // @todo This should be in a config/salesforce.php in the Salesforce package.
        $response = (new GuzzleClient())->post(env('SALESFORCE_LOGIN_URL'), [
            'form_params' => [
                'grant_type'    => 'password',
                'client_id'     => env('SALESFORCE_API_KEY'),
                'client_secret' => env('SALESFORCE_API_SECRET'),
                'username'      => env('SALESFORCE_API_USERNAME'),
                'password'      => env('SALESFORCE_API_PASSWORD'),
            ],
        ]);

        if (!($response instanceof Response)) {
            throw new \RuntimeException('Could not get a Salesforce OAuth2 token.');
        }

        $response = json_decode($response->getBody()->getContents(), true);
        if (empty($response['instance_url'])) {
            throw new SalesforceAPIException('Could not get a Salesforce instance URL.', SalesforceAPIException::API_AUTH);
        }
        $this->instanceURL = $response['instance_url'];

        return $response['access_token'];
    }

    protected function generateOAuth2TokenOptions(): array
    {
        static $authToken = null;

        if (!$authToken) {
            $authToken = $this->obtainOAuthToken();
        }

        return [
            'headers' => [
                'Authorization' => "Bearer {$authToken}",
            ],
        ];
    }
}

