<?php declare(strict_types=1);

/**
 * This file is part of the Salesforce PHP API Client, a PHP Experts, Inc., Project.
 *
 * Copyright © 2019 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *  GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *  https://www.phpexperts.pro/
 *  https://github.com/phpexpertsinc/Salesforce-API-Client
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\SalesforceClient;

use PHPExperts\RESTSpeaker\RESTSpeaker;
use PHPExperts\SalesforceClient\Managers\Account;

final class SalesforceClient
{
    /** @var RESTSpeaker */
    private $api;

    /** === Managers (See: Composition Architectural Pattern) === */

    /** @var Account */
    public $account;

    // @FIXME: Move this to its own ServiceProvider before PR.
    private function buildRestSpeaker(): RESTSpeaker
    {
        $restAuth = new SalesforceRESTAuth();

        $sfClient = new RESTSpeaker($restAuth->login(), $restAuth->getInstanceURL());

        return $sfClient;
    }

    // @FIXME: Use Dependency Injection.
    public function __construct()
    {
        $this->api = $this->buildRestSpeaker();

        $this->account = new Account($this, $this->api);
    }
}
