<?php declare(strict_types=1);

namespace PHPExperts\SalesforceClient\DTOs;

use PHPExperts\DataTypeValidator\DataTypeValidator;
use PHPExperts\SimpleDTO\SimpleDTO;
use PHPExperts\SimpleDTO\WriteOnce;

/**
 * @property string  $street          // "405 Fairbanks"
 * @property string  $city            // "Houston"
 * @property string  $state           // "Texas"
 * @property string  $postalCode      // "77009"
 * @property string  $country         // "United States"
 * @property string  $stateCode       // "TX"
 * @property string  $countryCode     // "US"
 * @property ?string $Latitude        // null
 * @property ?string $Longitude       // null
 * @property ?float  $GeocodeAccuracy // null
 */
class AddressDTO extends SimpleDTO
{
    use WriteOnce;
}
