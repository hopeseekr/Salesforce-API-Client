# Salesforce PHP API Client

An easy-to-use Salesforce API Client, a PHP Experts, Inc., project.

## Usage

```php
    
```

## Installation

Via Composer

```bash
composer require phpexperts/salesforce-api-client
```

## Change log

Please see the [changelog](CHANGELOG.md) for more information on what has changed recently.

## Testing

```bash
phpunit
```

## Contributing

Please take a look at [contributing.md](contributing.md) if you want to make improvements.


## Credits

- [Theodore R. Smith](https://www.phpexperts.pro/])

## License

MIT license. Please see the [license file](LICENSE) for more information.


[ico-version]: https://img.shields.io/packagist/v/phpexperts/conciseuuid.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/phpexperts/conciseuuid.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/phpexperts/conciseuuid/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/phpexperts/conciseuuid
[link-downloads]: https://packagist.org/packages/phpexperts/conciseuuid
[link-travis]: https://travis-ci.org/phpexperts/conciseuuid
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/phpexperts
[link-contributors]: ../../contributors]
